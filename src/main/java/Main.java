import entity.Group;
import entity.Student;

import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Student student = new Student(1, "Almat", "87086534886", 1905);
        Group group = new Group(1905,"CS");
        Connection conn = null;

        try {
            String url = "jdbc:postgresql://localhost:5432/post+gres";
            String user = "postgres";
            String password = "almat200202";


            conn = DriverManager.getConnection(url, user, password);

            Statement stmt = conn.createStatement();
            ResultSet rs;
            String query = "INSERT INTO Group VALUES" +
                    "("+group.id+","+group.name+")";
            stmt.executeUpdate(query);
            query = "INSERT INTO  Student VALUES " +
                    "("+student.id+","+student.name+","+student.phone+","+student.groupId+")";
            stmt.executeUpdate(query);
            Scanner in = new Scanner(System.in);
            String group_name = in.next();
            String query1 = "SELECT Student.id,Student.name,Student.phone" +
                    "FROM Student" +
                    "INNER JOIN Group" +
                    "ON Student.id = Group.id" +
                    "WHERE Group.name ="+group_name;
            rs = stmt.executeQuery(query1);
            while (rs.next() ) {
                System.out.println(rs.getString("id"));
                System.out.println(rs.getString("name"));
                System.out.println(rs.getString("phone"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }


    }

}
